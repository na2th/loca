# Miscelânia

## Como acessar o Mathscinet da minha casa?

Claro, esta dica não serve apenas para acessar o Mathscinet, mas
também para acessar qualquer site que espere que tu esteja conectado
pela USP.
A forma mais prática que temos até o momento é usar um proxy.

Temos uma solução direta para o firefox, mas com certeza devem ter
soluções análogas para outros navegadores.

Você pode instalar a extensão
[FoxyProxy](https://addons.mozilla.org/en-US/firefox/addon/foxyproxy-standard/)
e deixar que ela gerencie o restante.

Com a extensão instalada, você pode adicionar um perfil.
Um exemplo de configuração é:

![Configurações FoxyProxy](foxyproxyconfig.jpg)

Com isso, para acessar o Mathscinet, basta conectar por ssh pelo
terminal

    ssh -D 4321 nathan@ime.usp.br

e habilitar o proxy no FoxyProxy.
