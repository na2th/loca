# Zeladoria

Esta página tem as informações gerais sobre como cuidar e manter o bem
estar físico do laboratório.

* [Clique aqui para saber sobre a Hulk](Hulk.md)

##  Como consertar coisas no laboratório?

O IME tem uma seção chamada Seção de Serviços Gerais. Eles são os
responsáveis pelo bem estar físico do instituto. Já pedimos para eles
nos ajudarem com

* Troca de lâmpadas,
* Parafusos das cadeiras que caem.

Para conseguir ajuda deles, você deve abrir um pedido de serviço na
[página deles no IME](https://www.ime.usp.br/~ssg/servicos/index.php).

O site pede alguns dados:

* Nome
* Email
* Ramal
* Setor
* Sala e Bloco
* Solicitação

O Setor é `CCSL`, e o loca é a sala 119 do bloco C. O ramal é um
número de seis dígitos que pode ser visto no canto esquerdo alto da
tela do telefone (o que fica próximo a impressora).

Você vai receber um email confirmando o pedido. Em tese, depois de
atenderem o laboratório, eles também mandam um email avisando que o
serviço foi atendido. Ainda assim, **já aconteceu deles confirmarem
que o serviço foi feito antes de fazerem**. Não precisei reclamar,
eles acabaram vindo alguns dias depois, mas talvez seja interessante
ir lá perguntar.

A sala deles fica no bloco B, de frente para o estacionamento do IME.
O site também tem um telefone para contato.

## Como trocar o toner da impressora?
## Como patrimoniar algo?
## Biblioteca do LOCA
