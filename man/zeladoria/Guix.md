# Sobre o Guix

Nas palavras dele mesmo, o [GNU Guix](https://guix.gnu.org/) é

> Functional package manager for installed software packages and
> versions description: GNU Guix is a functional package manager for the
> GNU system, and is also a distribution thereof.
> It includes a virtual machine image.
> Besides the usual package management features, it also supports
> transactional upgrades and roll-backs, per-user profiles, and much
> more.
> It is based on the Nix package manager.

Em resumo, o Guix é um gerenciador de pacotes.

A ideia de instalar o Guix na Hulk é **permitir que os próprios
usuários gerenciem seus pacotes**, sem depender dos poucos e ocupados
admins.

Falando de outra maneira, os admins da Hulk são pessoas como você:
alunos ou pós-graduandos que, quando o trabalho permite, pegam um
tempo para cuidar da máquina.
Se os admins são como você, por reflexividade, você é como os admins,
e pode gerenciar seus própios pacotes.

Esse cenário é interessante por vários motivos:

* O usuário da Hulk não fica refém da disponibilidade dos admins;
* Quem usa os pacotes é o maior interessado que as coisas funcionem, e
  quem mais entende do pacote.
  Logo, é quem está melhor preparado para gerenciar o pacote.
* O Guix é projetado com preocupações sobre **reproducibilidade** e
  **isolamento** entre os usuários.
  Um pacote instalado pelo `Debian` pode ser atualizado pelo admin sem
  que você saiba e quebrar seu código, talvez até [numa semana crítica
  para você](#história-sobre-criar-pacotes).
  Ao mesmo tempo, um pacote instalado pelo `Guix` só pode ser
  atualizado por quem instalou.
* O Guix pode oferecer versões mais novas do que as disponíveis na
  Hulk pelo Debian.
  Por exemplo, em 2021-06-30, temos que os seguintes pacotes tem versões
  mais novas no Guix do que no Debian Buster:

   * GHC (8.4.4 vs. 8.8.3)
   * GAP (4r.10 vs. 4.11)
   * GCC (8 vs. 10.3.0)
   * OpenJDK (11 vs. 16.0.1)
   * Perl (5.28 vs. 5.30)
   * Python (3.7.3-1 vs. 3.8.2)
   * R (3.5.2 vs. 4.1.0)
   * Ruby (2.5.2 vs. 3.0.0 or 2.7.2)

  Se os usuários se entenderem com o Guix, é capaz dessa diferença ir
  ficando cada vez maior, pois os admins terão cada vez menos motivo
  para atualizar os pacotes do Debian.

Por fim, lembre-se sempre do [tio
Ben](https://en.wikipedia.org/wiki/With_great_power_comes_great_responsibility):
o **direito** de gerenciar seus próprios pacotes acompanha o **dever**
de gerenciar seus próprios pacotes.

# Instalar pacotes com Guix

Depois de ter [instalado o Guix para o seu
usuário](#instalação-do-gnu-guix-para-cada-usuário), você pode
procurar pacotes com

	$ guix package -s evince

e instalar com

	$ guix install evince

Pare remover um pacote, você pode rodar

	$ guix remove evince

# Coleta de Lixo

Pedimos que, toda vez que você **instalar** ou remover pacotes, que
você rode

	$ guix gc

para apagar arquivos que não são utilizados pelo teu perfil; os
pacotes do teu perfil podem ser vistos com

	$ guix package -I

Isto é importante pois se a Hulk começar a ficar com o HD cheio, os
admins terão que desinstalar pacotes dos usuários.
Um admin pode ver quais usuários estão usando mais memória olhando o
tamanho das pastas em `/var/guix/profiles/per-user/`.
É então natural desinstalar pacotes dos usuários que consumirem mais
memória.
Para evitar esse cenário infeliz, mantenha seu perfil limpo.

**Importante:** instalar pacotes pode ocupar memória além do
pacote instalado.
Pode haver dependências que o Guix instala apenas para compilar um
pacote que você deseja, e ele por padrão guarda essas dependências
para poder usar depois.
Assim, mesmo depois de apenas usar `guix install` pode ser que o `guix
gc` consiga remover arquivos não utilizados.

# Instalação do GNU Guix para cada usuário

Leia com calma o [getting
started](http://guix.gnu.org/en/manual/en/html_node/Getting-Started.html)
do [manual do Guix](http://guix.gnu.org/en/manual/en/).
Note que você **não** está usando o Guix System, que é o sistema
operacional baseado no Guix; você está usando uma instalação do Guix
no Debian.

Em especial, no seu primeiro uso do Guix, você precisa:

* Configurar seu próprio `.bashrc` para poder achar os pacotes que
  instalou, como descrito no [getting
  started](http://guix.gnu.org/en/manual/en/html_node/Getting-Started.html).
* Instalar os `locale`'s, como descrito
  [aqui](http://guix.gnu.org/en/manual/en/html_node/Application-Setup.html).
* As [atualizações](#atualização-de-pacotes-do-guix) dos teus pacotes
  são responsabilidades suas.

O ideal é que você leia esses links acima com calma para entender a
ferramenta que está usando.
Também temos uma [mini versão do getting
started](#getting-started-do-loca).

# Atualização de pacotes do Guix

Você pode atualizar os pacotes instalados pelo guix rodando o comando

	guix upgrade

Talvez você precise [atualizar o próprio Guix](#atualização-do-guix)
antes de conseguir atualizar seus pacotes.

# Atualização do Guix

Cada usuário que começa a usar o Guix usa a versão disponível na Hulk.

Contudo, talvez essa versão fique defasada.

Para resolver tal problema, o usuário pode usar o Guix do sistema para
instalar uma versão própria do Guix, mais atualizada.
Isso pode ser feito usando o comando `guix pull`, como descrito no
[getting
started](http://guix.gnu.org/en/manual/en/guix.html#Getting-Started).

Observe que isso implica umas configurações a mais no teu `.bashrc`.

# Getting Started do LOCA

**A melhor opção é sempre ver o manual oficial: ele tem tudo para
estar mais atualizado do que este arquivo**.

Ainda assim, para facilitar a adoção do Guix, eu (Nathan) vou
descrever como configurei meu guix no dia 2021-06-30, e instalei meu
primeiro pacote.

Primeiro, eu adicionei essas linhas ao final do meu arquivo
`~/.bashrc` **da Hulk, e não da rede IME**:

	export GUIX_PROFILE=$HOME/.guix-profile
	export GUIX_LOCPATH=$GUIX_PROFILE/lib/locale
	export INFOPATH=$HOME/.config/guix/current/share/info:$INFOPATH
	source "$GUIX_PROFILE/etc/profile"
	PATH=$HOME/.config/guix/current/bin:$PATH

Depois disso, eu desloguei e loguei de novo na máquina.
Para checar se as coisas deram certo, rodei

	$ env | grep -i guix

e vi que apareciam as variáveis relacionadas ao Guix.

Depois disso rodei

	$ guix pull

para obter a versão mais recente do Guix.

Rodando

	$ which guix

vi que estava usando uma cópia minha do guix, localizada na pasta do
meu usuário.

Posso então instalar meus pacotes.
Começo instalando os `locale`'s, como descrito [no manual do
Guix](http://guix.gnu.org/en/manual/en/html_node/Application-Setup.html)

	$ guix install glibc-locales

e então instalei e usei meu pacote:

	$ guix install ghc
	$ ghc --version

Claro, respeitando a orientação sobre [coleta de
lixo](#coleta-de-lixo), termino minha instalação rodando

	$ guix gc

# Criando pacotes para o Guix

É possível, e relativamente fácil, criar pacotes para o Guix.

Claro que isso envolve um entendimento mais profundo do Guix, e mais
conversas com o [Manuel](http://guix.gnu.org/en/manual/en/html_node/).

Na prática, você criar um pacote é você escrever um código em
[scheme](https://en.wikipedia.org/wiki/Scheme_(programming_language))
que compila o programa que você deseja instalar.
Existem algumas vantagens sobre compilar da source diretamente:

* Você já tem uma infra-estrutura pronta para explorar.
  Se o projeto usa o [Meson Build System](https://mesonbuild.com/), ou
  é um pacote em Python para ser compilado com
  [setup.py](https://docs.python.org/3/distutils/setupscript.html), ou
  ainda com o bom e velho [./configure && make && make
  install](https://git.savannah.gnu.org/cgit/guix.git/tree/guix/build-system/gnu.scm),
  já existem opções prontas e mantidas por outras pessoas para você
  usar.
  Veja a [lista de build-systems
  completa](https://git.savannah.gnu.org/cgit/guix.git/tree/guix/build-system).
* Ainda na questão de infraestrutura pronta, é bem fácil aproveitar o
  que já existe.
  Veja como o [evince](https://wiki.gnome.org/Apps/Evince) é compilado
  rodando `guix edit evince`.
  Este comando te mostra um arquivo enorme chamado `gnome.scm`, que
  descreve como compilar diversos programas da
  [Gnome](https://www.gnome.org/).
  Isso é muito útil caso você tenha problemas: se algo der errado na
  compilação, é possível que outro pacote no mesmo arquivo resolva o
  problema que você está enfrentando, e você possa se aproveitar disso.
* O código scheme descreve todo o processo de compilação. 
  Isso permite que você deixe ele num repositório git e vá melhorando
  interativamente, e consiga facilmente replicar em outra máquina.
* A compilação não afeta o restante do teu ambiente.
  Imagine que você precisa compilar um projeto que usa o Meson, que é
  implementado em Python.
  Normalmente isso implica que você precisaria instalar o Meson, e
  talvez atualizar seus pacotes em Python, talvez afetando versões de
  bibliotecas que tu usam em outros projetos.
  Isso não ocorre no caso do Guix: as dependências de um projeto são
  compiladas na hora que tu manda o guix compilar o pacote, e isso
  fica isolado da versão do Python que você usa no resto do sistema.
  Além disso, as compilações que falham não mudam em nada teu
  ambiente.

Os capítulos mais pertinentes do manual são [Programming
Interface](http://guix.gnu.org/en/manual/en/html_node/Programming-Interface.html)
e
[Contributing](http://guix.gnu.org/en/manual/en/html_node/Contributing.html).

### História sobre criar pacotes

Eu, Nathan, fui atrás do Guix depois de uma [atualização quebrar o
Poppler](https://gitlab.gnome.org/GNOME/evince/-/issues/1608) faltando
duas semanas para a entrega da minha dissertação de mestrado.
O Evince, que era o leitor de pdf que eu usava na época, começou a
crashar constantemente, e precisei migrar para o mupdf, o que quebrou
bastante meu fluxo de trabalho.
Eu poderia fazer um downgrade do Poppler, mas como ele também é
dependência do LaTeX, teria que fazer um downgrade de **todos** os
[3Gb de bibliotecas do
LaTeX](https://archlinux.org/groups/x86_64/texlive-most/).
Como o LaTeX é mais importante que o leitor de pdf nas semanas finais
da escrita de uma dissertação, não fiz o downgrade e precisei me
adaptar ao mupdf.
Além, claro, de aceitar as horas perdidas entendendo o que estava
acontecendo.

Entendo que no decorrer dos anos, é esperado que os softwares que uso
quebrem.
Busquei o Guix para ter mais controle sobre as atualizações das
ferramentas essenciais para o meu trabalho.
Basicamente, incorporei no meu trabalho a tarefa de configurar e
atualizar minhas ferramentas de trabalho, para poder **escolher**
quando que irei investir tempo nisso, ao invés de ficar refém de
atualizações do meu sistema operacional.
Meu pai, quando vendedor, tinha um carro da empresa que ele utilizava
para visitar clientes; os motivos pelos quais ele mantinha a
manutenção em dia são bem parecidos com o que estou descrevendo aqui.
Se o Guix não fizer nada além de deixar fora do meu ciclo padrão de
atualizações (no meu caso, `sudo pacman -Syyu`) os pacotes que uso
para trabalhar, já tenho uma vantagem.

Dito tudo isso, me surpreendi com o fato de ter rapidamente criado um
pacote novo.
Antes de ler completamente os capítulos mencionados acima, eu apenas
li com calma

* [Defining
  Packages](http://guix.gnu.org/en/manual/en/html_node/Defining-Packages.html);
* [Building from
  Git](http://guix.gnu.org/en/manual/en/html_node/Building-from-Git.html);
* [Running Guix Before It Is
  Installed](http://guix.gnu.org/en/manual/en/html_node/Running-Guix-Before-It-Is-Installed.html);
* [The Perfect
  Setup](http://guix.gnu.org/en/manual/en/html_node/The-Perfect-Setup.html); e
* O arquivo que descreve como compilar os
  [pacotes do
  Gnome](https://git.savannah.gnu.org/cgit/guix.git/tree/gnu/packages/gnome.scm).

Isso foi o suficiente para compilar a versão exata do evince que eu
desejava.
Essa modificação virou um
[patch](https://debbugs.gnu.org/cgi/bugreport.cgi?bug=49249) que
enviei para o próprio Guix.
Mesmo que esse patch não seja adotado pelo Guix (o que seria muito
legal!), eu já tenho a meu próprio repositório do Guix onde tenho
total controle para especificar meus pacotes.
Assim, a descrição exata das minhas ferramentas de trabalho fica
documentada num repositório, com um histórico que descreve minhas
escolhas e a facilidade de reverter mudanças.
Sendo mais explícito, além do `roll-back` oferecido pelo Guix, que é
parecido com o downgrade do `pacman` que não me salvou, ganhei mais um
nível de "volta no tempo" na descrição de como compilar o pacote que
criei.
Pra coroar, ainda fica extremamente simples de instalar a versão exata
de todos os softwares que eu uso: basta instalar o Guix, clonar meu
repositório pessoal, e instalar os pacotes.
