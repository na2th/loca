# Como atualizar a Hulk

1. Rode a atualização simples dos pacotes instalados.

        $ sudo apt-get update
        [. . .]
        $ sudo apt-get upgrade

   **Em geral, esse é o único passo necessário**.

   Se você quiser atualizar a versão do Debian, siga os próximos
   passos.

2. Verifique se uma atualização do Debian é de fato
   [necessária](#versão-do-debian).

3. Prepare sua forma de [documentar a atualização](#documente-a-atualização).

4. Copie para a sua máquina alguns arquivos importantes que podem ser
   sobrescritos na atualização.

    * Copie para a sua máquina o arquivo `/etc/sysctl.conf`.
    * Copie para a sua máquina o arquivo `/etc/ssh/sshd_conf`.

   Você pode copiar os arquivos da maneira que preferir; um exemplo de
   como fazer isso está documentado
   [aqui](#copiando-arquivos-importantes-para-sua-máquina).

5. Faça a atualização propriamente dita.

   No Debian é preciso editar manualmente o endereço dos repositórios,
   trocando o "codinome" da versão pelo da versão seguinte (por
   exemplo, trocar `jessie` por `stretch`).
   Isso é feito usando o comando a seguir.

        $ sudo apt edit-sources # edit the sources, changing jessie to stretch

   Atualize o Debian:

		sudo apt-get update
        sudo apt-get upgrade
        sudo apt-get dist-upgrade
		sudo apt-get full-upgrade

6. Resolva os conflitos nos arquivos de configurações que surgirem no
  passo 5.

   Durante a instalação pode ser necessário resolver conflitos em
   arquivos de configuração.
   Isso já aconteceu com os arquivos `/etc/sysctl.conf` e
   `/etc/ssh/sshd_config`, e por isso que o passo 4. pede que você
   copie esses arquivos para a sua máquina local.

   Algo parecido pode acontecer com outros arquivos.
   Quando esses conflitos ocorrem, você tem a opção de manter a
   versão atual, trocar, ver diferenças, etc.

   Uma opção razoável é fazer uma copia dos arquivos para sua máquina
   local e sobrescrever a versão atual (i.e, optar por pegar a nova
   versão desses arquivos).

   Talvez alguma coisa seja extremamente simples e você se sinta
   confortável de mudar na hora; isso também é razoável.

7. Retorne aos arquivos de configuração.

   Retorne aos arquivos de configuração que tiveram conflitos e
   reintroduza (nos lugares adequados) as configurações que haviam
   sido reescritas.

   Em `/etc/sysctl.conf`, as opções importantes foram as seguintes
   (parece que são proteções contra spoofing do servidor).
   
        net.ipv4.conf.default.rp_filter = 1
        net.ipv4.conf.all.rp_filter = 1
		[...]
		net.ipv4.ip_forward = 0
        
   O arquivo `/etc/ssh/sshd_config` exige mais atenção; siga as
   orientações sobre como [configurar o SSH abaixo](#ssh).

   Como mencionamos no passo 6., podem haver outros arquivos que você
   precise configurar; **faça isso agora**.

8. Verifique possíveis atualizações e reinicie o servidor.

        $ sudo apt-get full-upgrade
        $ sudo apt-get --purge autoremove
        $ sudo apt-get --purge autoremove
        $ sudo reboot

9. Verifique se a máquina está funcionando e atualize este
   passo-a-passo.

   Pode levar pouco mais do que 30 minutos para a Hulk começar a aceitar
   conexões por SSH, então não entre em pânico caso as coisas demorem
   a voltar.

   Se tudo correu bem, depois de um certo tempo você consegue acessar
   a Hulk por ssh de novo.

   * Caso algum arquivo de configuração *que não foi mencionado no passo
   4.* foi sobrescrito na atualização (veja o passo 6.), atualize o
   passo 4.

   * Atualize as informações sobre a [versão do
   Debian](#versão-do-debian) e [pacotes
   obsoletos](#pacotes-obsoletos).

   * Adicione as decisões que você tomou na atualização na seção [News
     das atualizações](#news-das-atualizações).

   * Atualize este arquivo de maneira apropriada.
   Sinta-se completamente livre para reorganizar este arquivo e torná-lo
   mais simples/claro.
   Logo após cumprir a atualização, fica evidente para ti as
   limitações do que está escrito aqui.

   Por mais importante que sejam os demais passos, note que eles
   apenas resolvem o problema que você tem nesse exato momento, até o
   Debian instalado se tornar obsoleto.

   Por outro lado, o passo-a-passo neste arquivo ajuda a resolver as
   coisas num prazo maior.

   Observe como esse arquivo lhe ajudou no processo.
   Imagine, por exemplo, como que seria atualizar a máquina sem saber
   que pode levar 30min para o SSH entrar no ar, e você passar esse
   tempo achando que quebrou as coisas.
   Ou como que o commando `sshd -t` é útil.
   Ou como que esse arquivo dá uma noção do trabalho envolvido na
   atualização e ajuda você a decidir quando será conveniente fazer a
   atualização.

10. **Parabéns, você atualizou a Hulk!**.

## Documente a atualização

A atualização pode levar um certo tempo, e alguns passos não são
triviais.
No mínimo, você precisa de uma maneira de lembrar as tarefas que
precisam ser feitas.

O passo 7. envolve editar diversos arquivos, e esses arquivos podem
surgir tanto no passo 4. quanto no passo 6.
Para um outro exemplo, na atualização de 2021-06-30 foi necessário
desinstalar o `sagemath` antes da atualização, para reinstalar no
final.
Como levou algumas horas o processo, seria muito fácil esquecer disso
sem anotar.

Também podem surgir decisões que precisam ser documentadas e
notificadas aos usuários.
Você pode ter que desinstalar certos pacotes, ou mudar algumas
configurações.
Em geral, não existe resposta correta, e o mais simples é apenas
documentar o que foi decidido, por que motivo, e convidar os usuários
a modificar.

Portanto, é bem razoável ter um arquivo de texto, ou mesmo um papel,
onde você anota essas coisas no decorrer da atualização.

E se você já irá ter esse papel, pense também no que pode ser
melhorado nesse passo a passo.

## Versão do Debian

Você só precisa atualizar o sistema se a versão do Debian estiver
atrasada.
Por isso, podemos manter nesse arquivo as informações sobre a versão
do Debian, e os links para ajudar a decidir se atualizar é necessário.

A página oficial do Debian diz [quais versões estão
disponíveis](https://www.debian.org/releases/).

De acordo com Long Term Support (LTS) deles,
[o Debian Buster, instalado em 2021-06-31, terá suporte até
2024-06](https://wiki.debian.org/LTS).

Os comandos abaixo checam quais as versões das coisas na máquina.

	$ cat /etc/issue
	Debian GNU/Linux 10 \n \l

	$ cat /etc/os-release
	PRETTY_NAME="Debian GNU/Linux 10 (buster)"
	NAME="Debian GNU/Linux"
	VERSION_ID="10"
	VERSION="10 (buster)"
	VERSION_CODENAME=buster
	ID=debian
	HOME_URL="https://www.debian.org/"
	SUPPORT_URL="https://www.debian.org/support"
	BUG_REPORT_URL="https://bugs.debian.org/"
	$ lsb_release -a
	No LSB modules are available.
	Distributor ID:	Debian
	Description:	Debian GNU/Linux 10 (buster)
	Release:	10
	Codename:	buster
	$ uname -a
	Linux hulk 4.19.0-17-rt-amd64 #1 SMP PREEMPT RT Debian 4.19.194-2 (2021-06-21) x86_64 GNU/Linux

## Pacotes obsoletos

Em tese, devemos desinstalar esses pacotes, e usar as versões mais
novas.

Os admins precisam criar uma política de desinstalar esses pacotes e
noticiar/negociar com os usuários.

Por hora, apenas listamos os pacotes aqui para que os usuários que
dependem destes pacotes saibam que é melhor abandonar o uso destes
programas.

	$ sudo aptitude search '~o'
	i A cpp-4.9                                                                     - GNU C preprocessor                                                                    
	i A cpp-6                                                                       - GNU C preprocessor                                                                    
	i A gcc-4.9                                                                     - GNU C compiler                                                                        
	i A gcc-4.9-base                                                                - GCC, the GNU Compiler Collection (base package)                                       
	i A gcc-6                                                                       - GNU C compiler                                                                        
	i A gcc-6-base                                                                  - GCC, the GNU Compiler Collection (base package)                                       
	i   iproute                                                                     - transitional dummy package for iproute2                                               
	i   libapt-inst1.5                                                              - deb package format runtime library                                                    
	i   libapt-pkg4.12                                                              - package management runtime library                                                    
	i A libasan1                                                                    - AddressSanitizer -- a fast memory error detector                                      
	i A libasan3                                                                    - AddressSanitizer -- a fast memory error detector                                      
	i   libboost-iostreams1.49.0                                                    - Boost.Iostreams Library                                                               
	i A libcloog-isl4                                                               - Chunky Loop Generator (runtime library)                                               
	i   libdb5.1                                                                    - Berkeley v5.1 Database Libraries [runtime]                                            
	i   libept1.4.12                                                                - High-level library for managing Debian package information                            
	i A libgcc-4.9-dev                                                              - GCC support library (development files)                                               
	i A libgcc-6-dev                                                                - GCC support library (development files)                                               
	i   libgcrypt11                                                                 - LGPL Crypto library - runtime library                                                 
	i   libgdbm3                                                                    - GNU dbm database routines (runtime version)                                           
	i   libgnutls26                                                                 - GNU TLS library - runtime library                                                     
	i A libisl10                                                                    - manipulating sets and relations of integer points bounded by linear constraints       
	i A libisl15                                                                    - manipulating sets and relations of integer points bounded by linear constraints       
	i A libjpeg8                                                                    - Independent JPEG Group's JPEG runtime library                                         
	i A libjpeg8-dev                                                                - Development files for the IJG JPEG library                                            
	i A libmpfr4                                                                    - multiple precision floating-point computation                                         
	i A libpng12-0                                                                  - PNG library - runtime                                                                 
	i A libpng12-dev                                                                - PNG library - development                                                             
	i   libprocps0                                                                  - library for accessing process information from /proc                                  
	i   libreadline6                                                                - GNU readline and history libraries, run-time libraries                                
	i A libruby2.1                                                                  - Libraries necessary to run Ruby 2.1                                                   
	i   libssl1.0.0                                                                 - Secure Sockets Layer toolkit - shared libraries                                       
	i A libssl1.0.2                                                                 - Secure Sockets Layer toolkit - shared libraries                                       
	i A libstdc++-4.9-dev                                                           - GNU Standard C++ Library v3 (development files)                                       
	i A libstdc++-6-dev                                                             - GNU Standard C++ Library v3 (development files)                                       
	i   libtasn1-3                                                                  - Manage ASN.1 structures (runtime)                                                     
	i   libudev0                                                                    - libudev shared library                                                                
	i   libxapian22                                                                 - Search engine library                                                                 
	i A linux-image-4.9.0-15-amd64                                                  - Linux 4.9 for 64-bit PCs                                                              
	i A linux-image-4.9.0-15-rt-amd64                                               - Linux 4.9 for 64-bit PCs, PREEMPT_RT                                                  
	i A linux-image-4.9.0-16-amd64                                                  - Linux 4.9 for 64-bit PCs                                                              
	i A linux-image-4.9.0-16-rt-amd64                                               - Linux 4.9 for 64-bit PCs, PREEMPT_RT                                                  
	i A openjdk-8-jre-headless                                                      - OpenJDK Java runtime, using Hotspot JIT (headless)                                    
	i A perl-modules-5.24                                                           - Core Perl modules                                                                     
	i A ruby2.1                                                                     - Interpreter of object-oriented scripting language Ruby                                
	i   sysvinit                                                                    - System-V-like init utilities - transitional package                                   

## Copiando arquivos importantes para sua máquina

Eu (Nathan, com usuário `nathan` tanto na rede IME quanto na Hulk), usei o scp.
Abri um terminal novo *na minha máquina* e rodei

	ssh ime.usp.br
	ssh hulk
	sudo cp /etc/ssh/sshd_conf ./sshd_conf # copia arquivo para meu usuário da Hulk
	sudo chown nathan ./sshd_conf          # me torna dono da copia local
	exit                                   # volta para a rede ime
	scp hulk:~/sshd_conf ./                # copia da hulk para a rede IME
	exit                                   # volta para minha maquina
	scp ime.usp.br:~/sshd_conf ./          # copia da rede IME para minha maquina

Com isso, tenho uma cópia local do arquivo `sshd_conf`.

## SSH

A forma como nós acessamos a Hulk é por SSH, e por isso devemos ter
muito cuidado ao configurá-lo.

A forma razoável de lidar com isso é ler com calma o arquivo
`/etc/ssh/sshd_config` criado na atualização e comparar com a cópia
feita no passo 4.

**Importante** como o próprio arquivo explica, as opções que estão
comentadas são o padrão do sistema e não precisam ser descomentadas.
Apenas descomente caso for mudar a opção para torná-la mais parecida
com a configuração anterior.
Por óbvio, adicione opções que estiverem faltando.

**Importante** as modifições do SSH só irão afetar a máquina após o
reboot.
Antes de fazer isso, você pode testar se as configurações estão
corretas e evitar eventuais dores de cabeça.
O comando

	sudo sshd -t

vai checar o arquivo de configuração e avisar sobre problemas.
Entenda quais opções que estão erradas e o motivo.
Talvez algumas simplesmente estejam `deprecated` e você não precise
fazer nada, talvez algumas sejam coisas que precisam ser removidas.

## News das atualizações

### 2021-06-30

#### GNU Guix

A principal novidade, que motivou a instalação.
A partir de agora, cada usuário pode gerenciar os próprios pacotes,
independente dos admins ou dos demais usuários.
[Saiba mais](Guix.md).

#### Linux Magic System Request Key Hacks

Durante a atualização, o Debian sugeriu trocar as permissões dadas ao
[Linux Magic System Request Key
Hacks](https://www.kernel.org/doc/html/latest/admin-guide/sysrq.html).
Como eu não faço a *menor* ideia do que é isso, acatei o que eles
indicaram e enviei email perguntando pra `admin@linux.ime.usp.br`.

#### HPN-SSH

As configurações relacionadas ao
[HPN-SSH](https://www.psc.edu/research/networking/hpn-ssh/) foram
removidas do SSH.

Aparentemente o `HPN-SSH` é uma versão paralela ao `openssh` com
o objetivo de tornar o `ssh` mais rápido.
Como eu não entendo do que isso se trata, me pareceu mais razoável
tirar essas opções e simplesmente usar o `openssh` padrão.
Qualquer usuário que entenda melhor do assunto e faça questão do
`HPN-SSH` está convidado à configurar as coisas de maneira apropriada.

Também deixei as opções `deprecated` no `/etc/ssh/sshd_config`.
