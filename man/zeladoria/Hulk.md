# Como acessar a Hulk?

Acessamos a Hulk via ssh.

A ideia é que você acesse a rede IME, e dela acesse a Hulk.

Vamos imaginar que:

* meu usuário na rede IME é `na2th`
* meu usuário na Hulk é `nathan`.

Posso acessar a Hulk usando

	ssh na2th@ime.usp.br # acesso a rede IME
	ssh nathan@hulk      # acesso a Hulk

ou, num comando só,

	ssh -J na2th@ime.usp.br nathan@hulk

Para facilitar minha vida, posso também editar o arquivo
`~/.ssh/config`.
Leia a documentação do [OpenSSH](https://www.openssh.com/), e em
especial [a opção
ProxyJump](https://en.wikibooks.org/wiki/OpenSSH/Cookbook/Proxies_and_Jump_Hosts#Passing_Through_One_or_More_Gateways_Using_ProxyJump).
Um exemplo simples de configuração é:

	Host ime
		HostName ime.usp.br
		User na2th

	Host hulk
		HostName hulk
		ProxyJump ime
		User nathan

Depois disso posso apenas rodar `ssh hulk` para acessar a máquina.

# Como instalar pacotes?

Existem duas formas de instalar pacotes na Hulk: usando o [GNU
Guix](#gnu-guix) ou [pacotes do Debian](#apt-get-ou-aptitude).

## GNU Guix

A forma mais simples de instalar um pacote é usando o [GNU
Guix](http://guix.gnu.org/).

Por exemplo, se você deseja instalar o Glasgow Haskell Compiler (GHC),
você pode simplesmente executar

	guix install ghc

Note que tu não precisa ser `root` ou usar o `sudo`; qualquer usuário
pode instalar pacotes pelo Guix, e a ferramenta garante que as
modificações de cada um não impactam o ambiente dos outros.

Em outras palavras, o que você instalar pelo Guix não deve estragar
com atualizações do sistema ou atividades de outros usuários, e você
não precisa entrar em contato com os admins para isso.

Existem algumas coisas que tu precisa arrumar na primeira vez que for
instalar os pacotes.
Leia como arrumar as coisas na [página do manual do LOCA sobre o
Guix](Guix.md).
Em especial, veja sobre a [instalação do
Guix](Guix.md#instalação-do-gnu-guix-para-cada-usuário).

## apt-get ou aptitude

Você também pode pedir para algum admin instalar o pacote para você.
A Hulk roda o Debian, então qualquer pacote do Debian pode ser
facilmente instalado por um usuário com privilégios sudo.

Contudo, os admins podem precisar atualizar teus pacotes para instalar
pacotes para outros usuários, ou talvez até precisem desinstalar algo
que você instalou.
Pacotes instalados pelo Guix ficam bem mais fáceis de você gerenciar e
evitar esses problemas.

# Como atualizar a Hulk

Rode a atualização simples dos pacotes instalados.

	$ sudo apt-get update
	$ sudo apt-get upgrade

**Em geral, esse é o único passo necessário**.

Eventualmente será necessário [atualizar a versão do
Debian](Debian-update.md).

