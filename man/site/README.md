# Como atualizar o site

As páginas do LOCA são os arquivos HTML contidos na
pasta `www` deste repositório.

# Atualizando a página inicial

Aqui estão os passos para mudar o conteúdo da [homepage do
LOCA](https://www.ime.usp.br/~loca).

## Pré-requisitos

Você precisa de duas coisas:

1. permissão de escrita no [repositório git do
   site](https://gitlab.com/na2th/loca)
2. permissão de login como usuário loca.

Para obter qualquer uma dessas permissões basta conversar com alguém
no Lab que já tenha essa permissão.

Existe muito material na internet [sobre
git](https://git-scm.com/book/en/v2) e [sobre
html](https://developer.mozilla.org/en-US/docs/Learn/HTML).

## Pondo a mão na massa

Vamos supor que tassio quer alterar a página
[index.html](https://www.ime.usp.br/~loca) do LOCA, para mudar a data de um
seminário de TACO.

Abrindo um terminal, ele digita os seguintes comandos

    $ git clone https://gitlab.com/na2th/loca.git loca_gitlab_repo
    $ cd loca_gitlab_repo
    $ nano www/index.html
    $ git add www/index.html
    $ git commit -m 'Change TACO seminar date'
    $ git pull
    Already up to date.
    $ git push origin master
    Username for 'https://gitlab.com': tassio
    Password for 'https://tassio@gitlab.com':
    Enumerating objects: 5, done.
    Counting objects: 100% (5/5), done.
    Delta compression using up to 8 threads
    Compressing objects: 100% (3/3), done.
    Writing objects: 100% (3/3), 376 bytes | 376.00 KiB/s, done.
    Total 3 (delta 1), reused 0 (delta 0)
    To https://gitlab.com/na2th/loca.git
       bcaccaa..044c2ec  master -> master
    $ ssh tassio@ime.usp.br 'ksu loca -e ~loca/gitlab_repo/loca_deploy.sh'

Explicando passo a passo:

1. Tássio faz uma cópia local do repositório (isso só é preciso a
primeira vez).
2. Ele entra na pasta do repositório.
3. Edita o arquivo `www/index.html`.
4. Marca o arquivo `www/index.html` para ser "commitado" no próximo
   commit.
5. Cria o commit, anotando o que foi mudado. "Change TACO..."
6. Verifica se alguém enviou mudanças desde a última vez que atualizou
   a cópia local do repositório.
7. Envia as mudanças para o repositório (o ksu pede a senha da rede
   ime, que hoje em dia pode ser diferente da senha do email
   @ime.usp.br).
8. Atualiza a página do LOCA.
