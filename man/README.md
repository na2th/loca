# Sobre o manual

A ideia é que o manual seja fácil de ler e editar. Para tal, iremos
nos aproveitar dos serviços do gitlab para mostrar arquivos markdown.

Clique [aqui](https://en.wikipedia.org/wiki/Markdown) para saber mais
sobre markdown.

Conforme formos precisando estruturar o site, podemos apenas criar
mais pastas dentro da `man` e colocar um arquivo `README.md` lá
dentro - de forma análoga aos `index.html` de um servidor comum.

## Seções do manual

* Como contribuir para o maloca? em `meta`
* Zeladoria em `zeladoria`
* Como atualizar `site`?
* Miscelania `misc`
