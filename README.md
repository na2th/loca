# Loca

Repositório que contém a infraestrutura do laboratório de Otimização,
Combinatória e Algoritmos do IME-USP.

Este repositório tem dois papéis:

* Guardar o blog do laboratório, que será sincronizado com o ~loca.
* Guardar o *maloca*, o Manual do Laboratório.

Para acessar o manual, acesse a pasta `man` deste repositório.
Para contribuir para a página, modifique a pasta `www` deste
repositório.
